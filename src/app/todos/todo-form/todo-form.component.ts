import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TodosService } from '../todos.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-todo-form',
    templateUrl: './todo-form.component.html',
    styleUrls: ['./todo-form.component.scss']
})
export class TodoFormComponent implements OnInit, OnDestroy {
    public todoForm: FormGroup;
    private id: number;
    private editMode = false;

    private subscription: Subscription;
    private subscriptionAdd: Subscription;
    private subscriptionEdit: Subscription;

    constructor(
        private todosService: TodosService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.id = +this.route.snapshot.paramMap.get('id');
        if (this.id !== 0) {
            this.editMode = true;
        }
        this.initForm();
    }

    public onSubmit(): void {
        const obs$ =  this.editMode ? this.todosService.updateTodo(this.id, this.todoForm.value) : this.todosService.addTodo(this.todoForm.value);
        this.subscriptionEdit = obs$.subscribe((response) => {
            console.log(response);
            this.onCancel();
        });
    }

    public onCancel(): void {
        if (this.editMode) {
            this.router.navigate(['../../'], { relativeTo: this.route });
        } else {
            this.router.navigate(['../'], { relativeTo: this.route });
        }
    }

    private initForm(): void {
        if (this.editMode) {
            this.subscription = this.todosService.getTodo(this.id).subscribe(todo => {
                this.todoForm = new FormGroup({
                    id: new FormControl(todo.id),
                    title: new FormControl(todo.title, Validators.required),
                    text: new FormControl(todo.text),
                    createdAt: new FormControl(todo.createdAt)
                });
            });
        } else {
            this.todoForm = new FormGroup({
                title: new FormControl('', Validators.required),
                text: new FormControl('')
            });
        }
    }

    ngOnDestroy(): void {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
        if (this.subscriptionEdit) {
            this.subscriptionEdit.unsubscribe();
        }
        if (this.subscriptionAdd) {
            this.subscriptionAdd.unsubscribe();
        }
    }
}
