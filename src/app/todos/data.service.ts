import { Injectable } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { Todo } from '../todo';
import { TodosService } from './todos.service';

@Injectable()
export class DataService {
    private dataSource = new BehaviorSubject<Array<Todo>>(new Array<Todo>());
    private subscription: Subscription;
    currentData = this.dataSource.asObservable();

    constructor(private todosService: TodosService) { }

    public reload(): void {
        this.unsubscribeData();
        this.subscription = this.todosService.getTodos().subscribe((data: Array<Todo>) => {
            this.dataSource.next(data);
        });
    }

    private unsubscribeData(): void {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

}
