import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Injectable } from '@angular/core';
import { ModalOptions } from './ModalOptions';
import { ModalContentComponent } from './modal-content/modal-content.component';

@Injectable()
export class ConfirmModalService {

    constructor(private modalService: NgbModal) {}

    public openConfirmModal(options: ModalOptions): Promise<any> {
    const modal = this.modalService.open(ModalContentComponent);
    modal.componentInstance.modalMessage = options.modalMessage;
    modal.componentInstance.modalTitle = options.modalTitle;
    modal.componentInstance.modalTitleClass = options.modalTitleClass;
    modal.componentInstance.confirmButtonClass = options.buttonClass;
    modal.componentInstance.confirmButtonIcon = options.buttonIcon;
    modal.componentInstance.confirmButtonText = options.buttonText;
    return modal.result;
    }

}

