export interface ModalOptions {
    modalTitle: string;
    modalTitleClass: string;
    modalMessage: string;
    buttonClass: string;
    buttonIcon: string;
    buttonText: string;
}
