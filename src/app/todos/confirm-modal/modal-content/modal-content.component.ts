import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-content',
  templateUrl: './modal-content.component.html',
  styleUrls: ['./modal-content.component.scss']
})
export class ModalContentComponent {
 public modalMessage: string;
 public modalTitle: string;
 public modalTitleClass: string;
 public confirmButtonClass: string;
 public confirmButtonIcon: string;
 public confirmButtonText: string;

  constructor(public activeModal: NgbActiveModal ) {}

}
