import { Component, Input, OnDestroy } from '@angular/core';
import { Todo } from '../../todo';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmModalService } from '../confirm-modal/confirm-modal.service';
import { ModalOptions } from '../confirm-modal/ModalOptions';
import { Subscription } from 'rxjs';
import { TodosService } from '../todos.service';
import { DataService } from '../data.service';


@Component({
    selector: 'app-todo-item',
    templateUrl: './todo-item.component.html',
    styleUrls: ['./todo-item.component.scss']
})

export class TodoItemComponent implements OnDestroy {
    @Input() todo: Todo;
    deleteSubscription: Subscription;
    completeSubscription: Subscription;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private modalService: ConfirmModalService,
        private todosService: TodosService,
        private dataService: DataService
    ) {}

    public onEdit(): void {
        this.router.navigate(['edit', this.todo.id], { relativeTo: this.route });
    }

    public onComplete(): void {
        const options: ModalOptions = {
            modalTitle : 'TODO Complete',
            modalTitleClass : 'modal-title text-success',
            modalMessage : 'Are you sure you want to complete this TODO?',
            buttonClass : 'btn btn-outline-success mr-2',
            buttonIcon: 'far fa-check-square',
            buttonText : 'Complete'
        };
        this.modalService.openConfirmModal(options).then((result) => {
            if (result === 'Confirmed') {
                this.callComplete();
            }
            console.log(result);
        });
    }

    private callComplete(): void {
        this.completeSubscription = this.todosService.completeTodo(this.todo.id).subscribe(() => {
            this.dataService.reload();
            }
        );
    }

    private callDelete(): void {
        this.deleteSubscription = this.todosService.deleteTodo(this.todo.id).subscribe(
            (reponse) => {
                this.dataService.reload();
            }
        );
    }

    ngOnDestroy(): void {
        if (this.completeSubscription) {
            this.completeSubscription.unsubscribe();
        }
        if (this.deleteSubscription) {
            this.deleteSubscription.unsubscribe();
        }
    }

    public onDelete(): void {
        const options: ModalOptions = {
            modalTitle : 'TODO Delete',
            modalTitleClass : 'modal-title text-danger',
            modalMessage : 'Are you sure you want to delete this TODO?',
            buttonClass : 'btn btn-outline-danger mr-2',
            buttonIcon: 'far fa-check-square',
            buttonText : 'Delete'
        };
        this.modalService.openConfirmModal(options).then((result) => {
            if (result === 'Confirmed') {
                this.callDelete();
            }
            console.log(result);
        });
    }

}
