import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Todo } from '../todo';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable()
export class TodosService {
    private baseUrl = environment.baseUrl;

    constructor(private httpClient: HttpClient) {
    }

    public getTodos(): Observable<Array<Todo>> {
        return  this.httpClient.get<Array<Todo>>(this.baseUrl);
    }

    public getTodo(id: number): Observable<Todo> {
        return this.getTodos().pipe(map(txs => txs.find(txn => txn.id === id)));
    }

    public addTodo(todo: Todo): Observable<Todo> {
        return this.httpClient.post<Todo>(this.baseUrl, todo);
    }

    public updateTodo(id: number, newTodo: Todo): Observable<any> {
        return this.httpClient.put<any>(this.baseUrl + '/' + id, newTodo);
    }

    public deleteTodo(id: number): Observable<any> {
        return this.httpClient.delete<any>(this.baseUrl + '/' + id);
    }

    public completeTodo(id: number): Observable<any> {
        return this.httpClient.post<Todo>(this.baseUrl + '/complete/' + id, null);
    }
}
