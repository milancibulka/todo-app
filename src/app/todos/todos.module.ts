import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { TodoFormComponent } from './todo-form/todo-form.component';
import { TodoItemComponent } from './todo-item/todo-item.component';
import { TodosComponent } from './todos.component';
import { TodosRoutingModule } from './todos-routing.module';
import { TodosService } from './todos.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DataService } from './data.service';
import { ModalContentComponent } from './confirm-modal/modal-content/modal-content.component';
import { ConfirmModalService } from './confirm-modal/confirm-modal.service';


@NgModule({
    declarations: [
        TodoFormComponent,
        TodoItemComponent,
        TodosComponent,
        ModalContentComponent
    ],
    imports: [
        CommonModule,
        NgbModule,
        ReactiveFormsModule,
        TodosRoutingModule
    ],
    providers: [
        TodosService,
        DataService,
        ConfirmModalService
    ],
    entryComponents: [
        ModalContentComponent
      ]
})
export class TodosModule { }
