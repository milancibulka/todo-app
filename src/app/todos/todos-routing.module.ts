import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TodosComponent } from './todos.component';
import { TodoFormComponent } from './todo-form/todo-form.component';

const todosRoutes: Routes = [
    {
        path: '', children: [
            { path: '', component: TodosComponent },
            { path: 'new', component: TodoFormComponent },
            { path: 'edit/:id', component: TodoFormComponent },
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(todosRoutes)
    ],
    exports: [RouterModule],
    providers: []
})
export class TodosRoutingModule { }
