import { Component, OnInit, OnDestroy } from '@angular/core';
import { Todo } from '../todo';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { DataService } from './data.service';

@Component({
    selector: 'app-todos',
    templateUrl: './todos.component.html',
    styleUrls: ['./todos.component.scss']
})
export class TodosComponent implements OnInit, OnDestroy {
    public todos: Array<Todo>;
    private subscription: Subscription;

    constructor(private dataService: DataService,
                private router: Router,
                private route: ActivatedRoute) { }

    ngOnInit() {
        this.dataService.reload();
        this.subscription = this.dataService.currentData.subscribe((todos: Array<Todo>) => {
            this.todos = todos;
        });
    }

    public onNewTodo(): void {
        this.router.navigate(['new'], { relativeTo: this.route });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
